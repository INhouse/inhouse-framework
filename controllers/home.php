<?php

require_once 'lib/maincontroller.php';


class Home extends MainController {
    protected $page = 'Inicio';

    protected $tpl = 'pages:home.html';

    public function index() {
        $vars = array();

        $vars['titulo'] = 'Hola Mundo';
        $vars['mensaje'] = 'Esta es una prueba de contenido';

        return $vars;
    }

    public function mensaje() {
        $this->setTpl('pages:texto.html');

        $vars = array();

        $vars['titulo'] = 'Esta es una prueba';
        $vars['cuerpo'] = 'lorem ipsum ...';

        return $vars;
    }
}
