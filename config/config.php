<?php

require 'moondragon/moondragon.render.php';
require 'moondragon/moondragon.session.php';
require 'moondragon/moondragon.manager.php';
require 'moondragon/moondragon.mailer.php';

Template::addDir('html');

function add_route($route, $controller, $default = false) {
    $file = 'controllers/'.strtolower($controller).'.php';
    Router::addSection($route, $file, $controller, $default);
}

function add_route_default($route, $controller) {
    add_route($route, $controller, true);
}

include 'config/vars.php';
include 'config/routes.php';
