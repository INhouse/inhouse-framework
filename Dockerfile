FROM php:5.6-apache

RUN apt-get update && apt-get install -y locales
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN echo "es_SV.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen

RUN docker-php-ext-install gettext

RUN a2enmod rewrite

WORKDIR /var/www/html

ADD . .
