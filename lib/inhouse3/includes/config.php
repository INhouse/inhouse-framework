<?php

require 'moondragon/moondragon.caller.php';

if(!defined('INHOUSE3_API_URL')) {
    define('INHOUSE3_API_URL', 'http://api.admininhouse.com/');
}

if(!defined('INHOUSE3_MEDIA_URL')) {
    define('INHOUSE3_MEDIA_URL', 'http://media.admininhouse.com/');
}

$dir = 'lib/inhouse3/includes';

require_once $dir.'/cachedcaller.php';
require_once $dir.'/baseapi.php';

include_once $dir.'/sections.php';
include_once $dir.'/pages.php';
