<?php


abstract class BaseAPI {
    protected $caller;

    protected $modulo;

    protected $suffix;

    protected $id_dominio;

    protected $token;

    protected $id_seccion;

    protected $data;

    protected $special_fields;

    public function __construct($id_dominio, $token, $id_seccion, $modulo = '', $preloaded_data = '') {
        $this->id_dominio = $id_dominio;
        $this->token = $token;
        $this->id_seccion = $id_seccion;
        if($modulo !== '') {
            $this->modulo = $modulo;
        }
        $this->suffix = $this->modulo;
        $this->caller = new CachedCaller($this->get_api_url());
        if($preloaded_data == '') {
            $this->refresh();
        }
        else {
            $this->data = $preloaded_data;
        }
    }

    public function refresh() {
        $this->data = $this->get_data();
    }

    protected function get_api_url() {
        $api_url = INHOUSE3_API_URL.'?sec=api&response='.$this->modulo;
        $api_url .= '&token='.$this->token.'&id_dominio='.$this->id_dominio;
        $api_url .= '&id='.$this->id_seccion.'&view=json';
        return $api_url;
    }

    protected function get_data(){
        $modulo = $this->modulo;
        $data = json_decode($this->caller->get());
        return $data->$modulo;
    }
}

class Element extends BaseAPI {
    public function get($field) {
        $valor = '';
        $field_suffix = $field.'_'.$this->suffix;

        if(isset($this->data->$field)) {
            $valor = $this->data->$field;
        }
        elseif(isset($this->data->$field_suffix)) {
            $valor = $this->data->$field_suffix;
        }
        else {
            $real_field = $this->match_field($field);
            if($real_field) {
                $valor = $this->data->$real_field;
            }
        }

        return $valor;
    }

    protected function match_field($field) {
        if(isset($this->special_fields[$field])) {
            return $this->special_fields[$field];
        }
        else {
            return NULL;
        }
    }
}


class Collection extends BaseAPI implements Iterator{
    protected $current_position = 0;

    protected $tipo;

    protected $id_elementos;

    protected $element_class = 'Element';

    public function current() {
        return $this->process_data($this->data[$this->current_position]);
    }

    public function key() {
        return $this->current_position;
    }

    public function next() {
        $this->current_position++;
    }

    public function rewind() {
        $this->current_position == 0;
    }

    public function valid() {
        return isset($this->data[$this->current_position]);
    }

    public function get_first() {
        return $this->process_data($this->data[0]);
    }

    public function get_position($position) {
        return $this->process_data($this->data[$position]);
    }

    public function get_count() {
        return count($this->data);
    }

    protected function process_data($data) {
        $id_data = $this->id_elementos;
        $class = $this->element_class;
        $element = new $class($this->id_dominio, $this->token, $data->$id_data, $this->tipo, $data);
        return $element;
    }
}
