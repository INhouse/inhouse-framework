<?php


class MediaAPI extends Element {
    protected $special_fields = array(
        'imagen' => 'ruta_media',
        'video' => 'media_media',
        'audio' => 'media_media'
    );

    protected $suffix = 'media';

    public function __construct($data) {
        $this->data = $data;
    }

    public function get_data() {
        return $this->data;
    }

    public function media_url($url) {
        return INHOUSE3_MEDIA_URL.$url;
    }

    public function get_imagen() {
        $url = parent::get('imagen');
        return $this->media_url($url);
    }

    public function get_thumb() {
        $url = parent::get('thumb');
        return $this->media_url($url);
    }

    public function get_video() {
        $url = parent::get('video');
        return $this->media_url($url);
    }
}


class MediaListAPI extends Collection {
    protected $modulo = 'medias';

    protected $tipo = 'media';

    protected $id_elementos = 'id_media';

    protected function process_data($data) {
        return new MediaAPI($data);
    }
}


class InternalGalleryAPI extends Element {
    protected $special_fields = array(
        'portada' => 'portada_thumb'
    );

    public function get_media($field) {
        $valor = parent::get($field);
        return INHOUSE3_MEDIA_URL.$valor;
    }

    public function get_portada() {
        return $this->get_media('portada');
    }

    public function get_portada_completa() {
        return $this->get_media('portada_imagen');
    }

    public function get_galeria() {
        $galeria = new MediaListAPI($this->id_dominio, $this->token, $this->get('id_galeria'));
        return $galeria;
    }
}

class GaleriaAPI extends InternalGalleryAPI {
    public function __construct($id_dominio, $token, $id_seccion, $modulo = '', $preloaded_data = '') {
        parent::__construct($id_dominio, $token, $id_seccion, 'galerias', $preloaded_data);
        $this->id_seccion = $this->data[0]->id_galeria;
        $this->modulo = 'galeria';
        $this->caller = new CachedCaller($this->get_api_url());
        $this->refresh();
    }
}


class GaleriaElementAPI extends InternalGalleryAPI {
    protected $modulo = 'galeria';
}


class GaleriasAPI extends Collection {
    protected $modulo = 'galerias';

    protected $tipo = 'galeria';

    protected $id_elementos = 'id_galeria';

    protected $element_class = 'GaleriaElementAPI';
}


class TextoAPI extends InternalGalleryAPI {
    protected $modulo = 'texto';
}


class TextosAPI extends Collection {
    protected $modulo = 'textos';

    protected $tipo = 'texto';

    protected $id_elementos = 'id_texto';

    protected $element_class = 'TextoAPI';
}


class NoticiaAPI extends InternalGalleryAPI {
    protected $modulo = 'noticia';
}


class NoticiasAPI extends Collection {
    protected $modulo = 'noticias';

    protected $tipo = 'noticia';

    protected $id_elementos = 'id_noticia';

    protected $element_class = 'NoticiaAPI';
}


class DocumentoAPI extends InternalGalleryAPI {
    protected $modulo = 'documento';
}


class DocumentosAPI extends Collection {
    protected $modulo = 'documentos';

    protected $tipo = 'documento';

    protected $id_elementos = 'id_documento';

    protected $element_class = 'DocumentoAPI';
}


class EventoAPI extends InternalGalleryAPI {
    protected $modulo = 'evento';
}


class EventosAPI extends Collection {
    protected $modulo = 'eventos';

    protected $tipo = 'evento';

    protected $id_elementos = 'id_documento';

    protected $element_class = 'EventoAPI';
}


class ProductoAPI extends InternalGalleryAPI {
    protected $modulo = 'producto';
}


class ProductosAPI extends Collection {
    protected $modulo = 'productos';

    protected $tipo = 'producto';

    protected $id_elementos = 'id_producto';

    protected $element_class = 'ProductoAPI';
}


class EnlaceAPI extends InternalGalleryAPI {
    protected $modulo = 'enlace';
}


class EnlacesAPI extends Collection {
    protected $modulo = 'enlaces';

    protected $tipo = 'enlace';

    protected $id_elementos = 'id_enlace';

    protected $element_class = 'EnlaceAPI';
}
