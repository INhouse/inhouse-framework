<?php


class MenuAPI extends Collection {
    protected $modulo = 'menu';

    protected $tipo = 'estructura';

    protected $id_elementos = 'id_estructura';
}


class PaginaAPI extends Element {
    protected $id_padre;

    public function __construct($id_dominio, $token, $id_seccion, $id_padre = 0) {
        parent::__construct($id_dominio, $token, $id_seccion);
        $this->id_padre = $id_padre;
        $this->suffix = 'estructura';
    }

    public function get_subseccion($id_seccion) {
        return new PaginaAPI($this->id_dominio, $this->token, $id_seccion, $this->data->id_estructura);
    }

    public function get_subsecciones() {
        return $this->get_elements();
    }

    public function get_elements($type = MenuAPI) {
        return new $type($this->id_dominio, $this->token, $this->data->id_estructura);
    }

    protected function get_api_url() {
        $api_url = INHOUSE3_API_URL.'?sec=api';
        $api_url .= '&token='.$this->token.'&id_dominio='.$this->id_dominio;
        if(is_string($this->id_seccion)) {
            $api_url .= '&response=slug_estructura&slug='.$this->id_seccion;
        }
        else {
            $api_url .= '&response=estructura&id='.$this->id_seccion;
        }
        if($this->id_padre) {
            $api_url .= '&id_padre='.$this->id_padre;
        }
        $api_url .= '&view=json';
        return $api_url;
    }
}
