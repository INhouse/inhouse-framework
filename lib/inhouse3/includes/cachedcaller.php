<?php


class CachedCaller extends Caller {
    protected static $cache = array();

    public function get() {
        if(!isset(self::$cache[$this->api_url])) {
            self::$cache[$this->api_url] = parent::get();
        }

        return self::$cache[$this->api_url];
    }
}
