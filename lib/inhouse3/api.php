<?php

require 'lib/inhouse3/includes/config.php';


class INhouse3API {
    protected $id_dominio;

    protected $token;

    public function __construct($id_dominio, $token) {
        $this->id_dominio = $id_dominio;
        $this->token = $token;
    }

    public function get($class, $id) {
        $api_class = $class.'API';
        return new $api_class($this->id_dominio, $this->token, $id);
    }

    public function getBanner($id) {
        return $this->get('GaleriaElement', $id);
    }

    public function getGaleria($id) {
        return $this->get('Galeria', $id);
    }

    public function getGalerias($id) {
        return $this->get('Galerias', $id);
    }

    public function getTexto($id){
        return $this->get('Texto', $id);
    }

    public function getNoticia($id) {
        return $this->get('Noticia', $id);
    }

    public function getNoticias($id) {
        return $this->get('Noticias', $id);
    }

    public function getDocumento($id) {
        return $this->get('Documento', $id);
    }

    public function getDocumentos($id) {
        return $this->get('Documentos', $id);
    }

    public function getEvento($id) {
        return $this->get('Evento', $id);
    }

    public function getEventos($id) {
        return $this->get('Eventos', $id);
    }

    public function getProducto($id) {
        return $this->get('Producto', $id);
    }

    public function getProductos($id) {
        return $this->get('Productos', $id);
    }

    public function getEnlace($id) {
        return $this->get('Enlace', $id);
    }

    public function getEnlaces ($id) {
        return $this->get('Enlaces', $id);
    }
}
