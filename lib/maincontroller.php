<?php

abstract class MainController extends Controller {
    protected $page;

    protected $tpl;

    protected function formatResponse($vars) {
        $response = Template::load($this->tpl, $vars, true);
        Buffer::write('output', $response);
        $tpl = Template::load('base.html', array('page' => $this->page), true);
        return $tpl;
    }

    protected function setTpl($tpl) {
        $this->tpl = $tpl;
    }
}
